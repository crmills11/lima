$.material.init();

var json = {
    title: "Listening Self-Assesment",
    showProgressBar: "top",
    pages: [{
        questions: [{
            type: "matrix",
            name: "lisening",
            
            title: "Read each statement and select the number that best reflects the frequency with which you demonstrate each listening skill. 1-Seldom 2-Sometimes 3-Frequently",
            columns: [{
                value: 1,
                text: "1"
            }, {
                value: 2,
                text: "2"
            }, {
                value: 3,
                text: "3"
            }],
            rows: [{
                value: "listen",
                text: "I listen for feelings and attitudes, as well as for facts."
            }, {
                value: "listen",
                text: "I listen for unspoken messages my reading body laguage and/or tone of voice."
            }, {
                value: "distracted",
                text: "I avoid being distracted, mentally and physically."
            }, {
                value: "snap",
                text: "I avoid making snap judgments and jumping to conclusions."
            }, {
                value: "response",
                text: "I avoid thinking of my response while the othe person is speaking."
            }, {
                value: "focus",
                text: "I focus on the content more than the delivery style."
            },{
                value: "paraphrase",
                text: "I paraphrase to confirm understanding."
            },{
                value: "suck",
                text: "I avoid getting sucked into the other persons emotions."
            },{
                value: "show",
                text: "I show the speaker Im listening with appropriate words and responses."
            },{
                value: "listen",
                text: "I listen more than I talk"
            },]
        }]
        }]
   }     
Survey.defaultBootstrapMaterialCss.navigationButton = "btn btn-green";
Survey.defaultBootstrapMaterialCss.rating.item = "btn btn-default my-rating";
Survey.Survey.cssType = "bootstrapmaterial";

var survey = new Survey.Model(json);

survey.onComplete.add(function(result) {

	var sum = 0;
	var rows = Object.values(result.data);
	for(var i = 0; i < rows.length; i++){
		var row = Object.values( rows[i] );
		for(var j = 0; j < row.length; j++) {
			var val = row[j];
			sum += parseInt(val);
		}
	}


function myFunction(sum) 

{
    var scoring;
    if (sum <= 30 && sum >= 24) {
        scoring = "You’re a professional listener! And people who converse with you likely know. Effective listening is a very valuable skill, in any area of life. Think about how you might set an example for coworkers, friends, and others who aren’t as effective listeners.";
    } else if (sum <= 23 && sum >= 16) {
        scoring = "You can listen, but how often? Maybe normal conversations are cake, but difficult conversations close your ears? We can all listen better, what’s hard is figuring out _how_ we can improve. Try to think of what makes you tune out or resist.";
    } else if (sum <= 15 && sum >= 10) {
        scoring = "You might see yourself as a “good listener,” but listening doesn’t necessarily mean _hearing_ the speaker. Try to think of what you appreciate in someone who’s listening to you. Do you practice those aspects yourself? Think about how you can listen better today.";
    } else {
        scoring = "Error";
    }
    
document.querySelector('#result').innerHTML = "Your Score " + sum + "<br>" + scoring;

} 

myFunction(sum);

});
       
survey.render("surveyElement");